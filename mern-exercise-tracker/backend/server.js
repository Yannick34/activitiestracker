//importation des modules
const cors = require("cors");
require("dotenv").config();
const mongoose = require("mongoose");
const express = require("express");
const app = express();

const port = process.env.port | 5000;

//middlewares
app.use(cors());
app.use(express.json());

//connection to DB

const uri = process.env.ATLAS_URI; //database uri given by atlas dashboard
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true}); //some former node.js modules for mongoDB was deprecated and these flags allow to not drop errors
const connection = mongoose.connection;
connection.once('open', () => {
   console.log('MongoDB database connection established successfully');
});


//routes
const exercisesRouter = require('./routes/exercises.js');
const usersRouter = require('./routes/users.js');

app.use('/exercises', exercisesRouter);
app.use('/users', usersRouter);

//Listening
app.listen(port, () => {
   console.log(`Server is running on port ${port}`);
});